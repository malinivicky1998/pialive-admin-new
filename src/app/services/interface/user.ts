export interface user {
  id: number;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

export interface userList {
  result: user[];
  page: number;
  per_page: number;
  support: {
    url: string,
    text: string
  };
  total: number;
  total_pages: number;
}
export interface userCredentials {
  email: string;
  password: string;
}

export interface userData {
  name: string,
  job: string
}

export interface errorReponse {
  error: {
    error: string;
  };
}
export interface updateUserResponse {
  edited: boolean,
  data: userData
}
export interface updateUserDetailResponse {
  name: string,
  job: string,
  updatedAt: Date
}
export interface loginReponse {
  token: string;
  statusCode:any;
  message:string
}

