import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class SpinnerService {
    isLoading = new BehaviorSubject<boolean>(false);
    isShown = new BehaviorSubject<boolean>(true);
    isCreate = new BehaviorSubject<boolean>(true);
    userDetail= new BehaviorSubject<any>([]);
    show() {
        this.isLoading.next(true);
    }
    hide() {
        this.isLoading.next(false);
    }
}