import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";

@Injectable({
    providedIn: 'root'
})

export class AuthGaurd implements CanActivate {

    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

        if (this.isUser() &&( state.url == "/user" )) {
            return true
        } 
        else if(this.isUser() &&  state.url=='/user/user-form'){
            return true
        }
        else {
            this.router.navigateByUrl("/login")
            return false;
        }
    }
    isUser() {
        return localStorage.getItem('accessToken') ? true : false;
    }
}