
import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";
import { userData, userCredentials } from "./interface/user";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})


export class ReqrescellService {

    constructor(public http: HttpClient) { }

    //login API
    login(data: userCredentials): Observable<any> {
        return this.http.post(environment.BASE_URL + '/login', data);
    }

    //get userlist API
    getUsersList(pageCount?: number): Observable<any> {
        const currentPageCount = pageCount ? pageCount : 1;
        return this.http.get(environment.ADMIN_URL + `/getAllUsers?page=${currentPageCount}`);
    }

    //create user API
    createUser(data:any){
        return this.http.get(environment.BASE_URL + `signup`, data);
    }

    //delete UserList API
    deleteUser(userId: number): Observable<any> {
        return this.http.delete(environment.BASE_URL + `users/${userId}`);
    }

    //update UserList API
    updateUser(data: userData, id: number): Observable<any> {
        return this.http.put(environment.BASE_URL + `users/${id}`, data);
    }

    //delay userList API
    getDelayUser() {
        return this.http.get(environment.BASE_URL + `users?delay=3`);
    }
}