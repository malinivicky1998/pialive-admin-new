import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ReqrescellService } from 'src/app/services/reqrescell.service';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  //local variable declaration
  public UserForm: FormGroup;
  private readonly subscription: Subscription[] = [];
  facebookMarked = false;
  faceBook = false;
  googleMarked = false;
  google = false;
  isCreate = true;
  radioItems: Array<string>;
  model = { option: '' };

  constructor(private fb: FormBuilder, private router: Router,
    public Reqrescell: ReqrescellService, private toastr: ToastrService, private spinner: SpinnerService) {
    this.radioItems = ['Male', 'Female', 'Others'];
    this.spinner.isShown.next(false)

  }

  ngOnInit() {

    //Form validation
    this.UserForm = this.fb.group({
      username: ["", [Validators.required]],
      name: ["", [Validators.required]],
      phone: ["", [Validators.required]],
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
      DOB: ["", [Validators.required]],
    });

    this.spinner.isCreate.subscribe((res: boolean) => {
      this.isCreate = res;
    });
    this.spinner.userDetail.subscribe((res: any) => {
      if(Object.keys(res).length>0){
  //    this.UserForm = res;
          //patch values
     this.UserForm.patchValue({
      name: res.name,

    })
    // username:res.username,
    // DOB:res.DOB,
    // email:res.email,
    // phone: res.phone,
      }
    });

  }

  //Facebook Signin
  faceBookVisibility(event) {
    this.facebookMarked = event.target.checked;
  }
  //Google Signin
  googleVisibility(event) {
    this.googleMarked = event.target.checked;

  }
  //sumbit the user
  CreateUser() {
    if (this.UserForm.invalid) {
      return this.UserForm.controls;
    }
    else {
      const formData = {
        email: this.UserForm.value.email,
        password: this.UserForm.value.password,
        username: this.UserForm.value.username,
        name: this.UserForm.value.name,
        phone: this.UserForm.value.phone,
        gender: this.model.option,
        DOB: this.UserForm.value.DOB,       
      };
      const userList: Subscription = this.Reqrescell.createUser(formData).subscribe((data: any) => {
        if (data.success) {
          this.toastr.success(data.message);
          this.router.navigate(['user']);
        }
        else {
          this.toastr.error(data.message);
        }
      },
        (err) => {
          this.toastr.error('Unable to create user ');
        }
      );
      this.subscription.push(userList)

    }
  }

  //Update the user
  UpdateUser() {
    if (this.UserForm.invalid) {
      return this.UserForm.controls;
    }
    else {
      const formData = {
        email: this.UserForm.value.email,
        password: this.UserForm.value.password,
        username: this.UserForm.value.username,
        name: this.UserForm.value.name,
        phone: this.UserForm.value.phone,
        country_code: this.UserForm.value.country_code,
        gender: this.model.option,
        DOB: this.UserForm.value.DOB,
        avatar: this.UserForm.value.avatar,
        facebook_signin: this.facebookMarked,
        google_signin: this.googleMarked,
      };
      const userList: Subscription = this.Reqrescell.createUser(formData).subscribe((data: any) => {
        if (data.success) {
          this.toastr.success(data.message);
          this.router.navigate(['user']);
        }
        else {
          this.toastr.error(data.message);
        }
      },
        (err) => {
          this.toastr.error('Unable to create user ');
        }
      );
      this.subscription.push(userList)

    }
  }

  CancelUser() {
    this.UserForm.patchValue({
      username: "",
      name: "",
      phone: "",
      email: "",
      password: "",
      country_code: "",
      DOB: "",
      avatar: '',
      google_in: false,
      facebook: false
    })
  }

}
