import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { ReqrescellService } from 'src/app/services/reqrescell.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { errorReponse, loginReponse } from 'src/app/services/interface/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  //local variable decalaration
  public loginForm: FormGroup;
  public submitted: boolean = false;
 // public email = '^([a-zA-Z0-9_\\-\\.\\+]+)@([a-zA-Z0-9_\\-\\.]+)\\.(\\b(?!web\\b)[a-zA-Z]{2,5})$'
  public email = ''
  private readonly subscription: Subscription[] = [];

  constructor(private fb: FormBuilder, private router: Router,
    public Reqrescell: ReqrescellService, private toastr: ToastrService) {
  }

  ngOnInit() {

    //form validation
    this.loginForm = this.fb.group({
      email: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });

  }
  get f() {
    return this.loginForm.controls;
  }
  login() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      console.log(this.loginForm.controls);
      this.toastr.error('Please Enter Valid Data');
    }
    else {
      const formData = {
        email: this.loginForm.value.email,
        password: this.loginForm.value.password,
      };
      const userLogin: Subscription = this.Reqrescell.login(formData).subscribe((data: loginReponse) => {
        if (data) {
          if(data.statusCode){
            this.toastr.error(data.message);
          }
          else{
          localStorage.setItem('accessToken', data.token);
          this.toastr.success('User Login Sccessfully');
          this.router.navigateByUrl('/user');
          }
        }
      }, (err: errorReponse) => {
        this.toastr.error(err.error.error);
      }
      );
      this.subscription.push(userLogin)
    }
  }

  //Unsubscribe
  ngOnDestroy() {
    this.subscription.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    })
  }
}
