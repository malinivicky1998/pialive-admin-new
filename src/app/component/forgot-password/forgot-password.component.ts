import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  loading = false;
  pageLoader = false;
  valid = false;
  enableLogin = false;
  value = ''
  passwordInputType = 'password';
  passwordInputType1 = 'password';
  loginModel = {
    email: '',
    password: '',
    rememberMe: false,
  };
  errors: string[] = [];
  forgotPassword: boolean = true;
  forgotEmail = '';
  successReset: boolean = false;
  setPass: boolean = false;
  resultData: boolean = false;
  setPassword = '';
  confirmPassword = '';
  otpData: boolean= false;
  otp:string ='';
  resetPassword:boolean = false;

  constructor(public http: HttpClient, private toastr: ToastrService, private router: Router,) { }

  ngOnInit(): void {
  }
  togglePasswordInputType(type: any = "") {
    if (type === 'confirm') {
      if (this.passwordInputType1 == 'password') {
        this.passwordInputType1 = 'text';
      } else {
        this.passwordInputType1 = 'password';
      }
    }
    else {
      if (this.passwordInputType == 'password') {
        this.passwordInputType = 'text';
      } else {
        this.passwordInputType = 'password';
      }
    }
  }
  setNewPassword() {
    if (this.setPassword !== this.confirmPassword) {
      this.errors = [];
      this.errors.push("Password and Confirm password must be same!")
    }
    else {

      this.loading = true;
      if (this.setPassword.trim() !== '' && this.confirmPassword.trim() !== '') {
        let url = environment.BASE_URL + `/resetPassword`
        let token = localStorage.getItem('accessToken');
        this.http.post(url, { 'token': token, 'password': this.setPassword.trim(), 'confirmPassword': this.confirmPassword.trim() }).subscribe((result: any) => {
          if (result.statusCode) {
            this.loading = false;
            this.toastr.success(result.message);
          }
          else {
            this.loading = false;
            this.toastr.success('Password Updated Sccessfully');
            this.router.navigateByUrl('/login');
          }
        },
          (error) => {
            let data = error.error?.error ? error.error?.error : (error.error?.detail ? error.error?.detail : (error.error?.details ? error.error?.details : ``))

            this.resultData = false;
            this.loading = false;
          });
      }
    }
  }
  forgotPasswordLogin(value:any='') {
    this.errors = [];
    this.loading = true;
    if (this.forgotEmail.trim() === '') {
      this.errors = [];
      this.errors.push("Email should not be empty!")
    }
    else {

      this.loading = true;
      let type:any
      if (this.forgotEmail.trim() !== '') {
        let data = this.validateEmail(this.forgotEmail)
        if(data === true){
          
            type = { 'email': this.forgotEmail.trim() }
          
        }
        else{
          type = { 'phone': this.forgotEmail.trim() }

        }
        let url = environment.BASE_URL + `/forgotPassword`
        this.http.post(url, type).subscribe((result: any) => {
          if (result.statusCode) {
            this.loading = false;
            this.toastr.error(result.message);
          }
          else {
            this.loading = false;
            this.forgotPassword = false;
            this.otpData = true
            localStorage.setItem('accessToken', result.message)
            this.toastr.success('Password Updated Sccessfully');
          //  this.router.navigateByUrl('/login');
          }
        },
          (error) => {
            let data = error.error?.error ? error.error?.error : (error.error?.detail ? error.error?.detail : (error.error?.details ? error.error?.details : ``))

            this.resultData = false;
            this.loading = false;
          });
      }
    }
  }

  verifyOtp(value:any='') {
    this.errors = [];
    this.loading = true;
    if (this.otp.trim() === '') {
      this.errors = [];
      this.errors.push("OTP should not be empty!")
    }
    else {

      this.loading = true;
      let type:any
      let token = localStorage.getItem('token')
      if (this.otp.trim() !== '') {
        type = { 'otp': this.otp.trim() , 'token':token}
        let url = environment.BASE_URL + `/resetPasswordVerifyOtp`
        this.http.post(url, type).subscribe((result: any) => {
          if (result.statusCode) {
            this.loading = false;
            this.toastr.error(result.message);
          }
          else {
            this.loading = false;
            this.otpData = false;
            this.resetPassword = true;
            this.toastr.success('Password Updated Sccessfully');
          }
        },
          (error) => {
            let data = error.error?.error ? error.error?.error : (error.error?.detail ? error.error?.detail : (error.error?.details ? error.error?.details : ``))

            this.resultData = false;
            this.loading = false;
          });
      }
    }
  }
  validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
  }
}
