import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { user, userList } from 'src/app/services/interface/user';
import { ReqrescellService } from 'src/app/services/reqrescell.service';
import { MatDialog } from '@angular/material/dialog';
import { SpinnerService } from 'src/app/services/spinner.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  //local varaiable declartion
  public users: user[] = [];
  public allUserList: user[] = [];
  private readonly subscription: Subscription[] = [];
  public listPageNumber: number = 1;

  constructor(private router: Router,
    public Reqrescell: ReqrescellService, private toastr: ToastrService, private dialog: MatDialog, private spinner:SpinnerService) {
      this.spinner.isShown.next(true)

     }

  ngOnInit() {
    this.getUserList(this.listPageNumber);
  }

  //Getting user list
  getUserList(id: number) {

    const userList: Subscription = this.Reqrescell.getUsersList(id).subscribe((data: userList) => {
      this.users = data.result;
      this.allUserList = data.result;
      this.toastr.success('User list fetch Sccessfully');
    },
      (err) => {
        this.toastr.error('Unable to fetching user list');
      }
    );
    this.subscription.push(userList)

  }

  //edit page navigation
  editUser(event:any) {
    this.spinner.isCreate.next(false);
    this.spinner.userDetail.next(event)
    this.router.navigate(['user/user-form']);
  }

  //create page navigation
  creteUser(){
    this.spinner.isCreate.next(true);
    this.router.navigate(['user/user-form']);

  }

  //Delete the user
  deleteUser(id: number) {
    const deleteUser: Subscription = this.Reqrescell.deleteUser(id).subscribe((data: any) => {
      this.toastr.success('User deleted Sccessfully');
    }, (err) => {
      this.toastr.error('User detail deletion failed');
    });
    this.subscription.push(deleteUser)
  }

  //Search option
  filterUser(event: any) {
    this.users = this.allUserList;
    let topic = event.value;
    if (topic && topic.trim() !== '') {
      this.users = this.users.filter((filter: any) => {
        return ((filter.first_name.toLowerCase().indexOf(topic.toLowerCase()) > -1) || (filter.last_name.toLowerCase().indexOf(topic.toLowerCase()) > -1) || (filter.email.toLowerCase().indexOf(topic.toLowerCase()) > -1))
      });
    }
    else {
      this.users = this.allUserList;
    }
  }

  //logout
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }


  // unsubscribe the list
  ngOnDestroy() {
    this.subscription.forEach(subscription => {
      if (subscription) {
        subscription.unsubscribe();
      }
    })
  }

}
