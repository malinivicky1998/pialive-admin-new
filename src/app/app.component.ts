import { AfterViewChecked, ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { SpinnerService } from './services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, AfterViewChecked {

  public isLoading = false;
  public isShowing = true;

  constructor(private loader: SpinnerService, private cdRef: ChangeDetectorRef) { }

  ngOnDestroy(): void {
    this.isLoading = false;
  }
  ngOnInit(){
    this.loader.isShown.subscribe((res: boolean) => {
      this.isShowing = res;
      
    });
  }
  ngAfterViewChecked(): void {
    this.loader.isLoading.subscribe((res: boolean) => {
      this.isLoading = res;
      if (!this.cdRef['destroyed']) {
        this.cdRef.detectChanges();
      }
    });
  }

}
